package cnu.andriyh.pop.lab1;

public class BreakThread implements Runnable {

    private final int sleepTime;
    private final Permission permission = new Permission();

    public BreakThread(int sleepTime) {
        this.sleepTime = sleepTime * 1000;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            throw new RuntimeException("Керуючий потік перервано!");
        }
        synchronized (permission) {
            permission.canStop = true;
        }
    }

    public synchronized Permission getPermission() {
        return this.permission;
    }


    class Permission {
        private boolean canStop = false;
        public boolean canStop() {
            return canStop;
        }
    }


}
