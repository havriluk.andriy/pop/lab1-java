package cnu.andriyh.pop.lab1;

public class Runner implements Runnable {

    private final BreakThread control;
    private final String name;

    public Runner(String name, BreakThread control) {
        this.name = name;
        this.control = control;
    }
    @Override
    public void run() {
        long sum = 0;
        long steps = 0;
        while (!control.getPermission().canStop()) {
            sum += 2;
            steps++;
        }
        System.out.println("Потік: [" + name + "] зупинено з сумою: " + sum + ", кількістю кроків: " + steps);
    }
}
