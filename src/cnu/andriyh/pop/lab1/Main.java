package cnu.andriyh.pop.lab1;

public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Відсутні аргументи програми - кількість потоків та час очікування в секундах");
            return;
        }
        int threads = Integer.parseInt(args[0]);
        int time = Integer.parseInt(args[1]);

        BreakThread breakThread = new BreakThread(time);
        System.out.println("Запускаємо потоки...");
        for (int i = 0; i < threads; i++) {
            new Thread(new Runner("-" + (i+1) + "-", breakThread)).start();
        }

        new Thread(breakThread).start();
        System.out.println("Потоки запущено, очікуйте...\n");
    }
}
